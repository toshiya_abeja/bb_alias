# Installation
cp bb_alias/bb /usr/local/bin

# Usage
In git repository, its fetch URL is bitbucket...

```
bb [cmd]
```

# cmd list

pr) jump to pull-requests

prn) jump to new pull-request

src) jump to src

ci) jump to commit

br) jump to branches

is) jump to issue

wiki) jump to wiki

dl) jump to downloads
