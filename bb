BASE_URL=https://bitbucket.org/`git remote -v | grep fetch | awk '{print $2}' | awk -F ':' '{print $2}' | awk -F '.' '{print $1}'`

case $1 in
    pr) open $BASE_URL/pull-requests;;
    prn) open $BASE_URL/pull-request/new;;
    src) open $BASE_URL/src;;
    ci) open $BASE_URL/commits;;
    br) open $BASE_URL/branches;;
    is) open $BASE_URL/issues?status=new&status=open;;
    wiki) open $BASE_URL/wiki/Home;;
    dl) open $BASE_URL/downloads;;
    *) open $BASE URL
esac
